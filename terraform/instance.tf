resource "digitalocean_droplet" "sarge" {
  name        = "sarge"
  region      = "ams2"
  size        = "${var.size}"
  resize_disk = "false"
  image       = "ubuntu-14-04-x64"
}
