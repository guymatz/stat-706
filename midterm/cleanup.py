#!/usr/bin/env python
# coding: utf-8
from robobrowser import RoboBrowser
import re
import sys

gender = sys.argv[1]  # call this script with gender as patameter, e.g. ./cleanup women
browser = RoboBrowser(history=True, parser="html.parser")
browser.open("http://www.mastersathletics.net/fileadmin/html/Rankings/All_Time/100metres%s.htm" % gender)
rows = browser.find_all('tr')


def row2list(row):
        """
        :param row: BeautifulSoup "row" of td elements (from a table)
        :return: list of entries from the table, all cleaned up and purdy
        """
        data = []
        for elem in row.find_all('td'):
            data.append(elem.getText().strip().encode('utf-8'))
        return data


csv_data = ''
        
sex = 'NA'
age = 'NA'
# Set header fields as first set of records
running_times = ["Time, Wind, Athlete, Nation, DOB, Venue, Date,Gender, Age"]
for row in rows:
    elements = row2list(row)
    try:
        pass  # use the next line for debugging output
        print(elements)
    except Exception as e:
        print("Are you using python3?  Use python 2.")
        sys.exit(1)
    # This stanza gets the sex and age for us from the "Header" HTML sections
    if len(elements) == 1 and re.match("^100 meter Dash", elements[0].strip()):
        contest = elements[0]
        try:
            # Find the sex and age in the header
            pattern = '^.* - (.) (..) - .*$'
            sex, age = re.match(pattern, contest.strip()).groups()
        except Exception as e:
            print("* contest = %s: %s" % (contest, e))
            continue
    elif len(elements) == 7:
        # The records we're interested in have 7 fields
        try:
            # The first field should be a number
            float(elements[0])
        except ValueError:
            print("** not including : %s" % elements)
            continue
        # converting elements list to record
        record = [x.strip() for x in elements]
        try:
            float(record[0])
        except ValueError:
            print("***  not including : %s" % record)
            continue
        # Set tail wind to 0, if it's empty
        if record[1] == '':
            record[1] = '0'
        # Not including records with tailwinds greate than 2.0
        try:
            if float(record[1]) > 2.0:
                print("****  not including : %s" % record)
                continue
        except ValueError:
            print("*****  not including : %s" % record)
            continue
        record.append(sex)
        record.append(age)
        # Create a CSV record
        csv_line = ",".join(record).replace('\r\n', ' ')
        running_times.append(csv_line)

with open('%s.csv' % gender, 'w') as csv_file:
    csv_file.write("\n".join(running_times))
