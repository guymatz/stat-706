---
title: "Homework 1"
author: "Guy Matz"
date: "9/16/2017"
output:
  html_document: default
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Summary of Correlation Coefficients

Correlation Coefficient[^1] is a statistical measure for how strongly  two "variables" relate to each other, i.e. how much of an effect changes to one variable has on another and vice-versa.  This measurement can indicate a relationship between the two set of data points which can then be used to predict other, as yet untested, data sets.  The most commonly used of these is the Pearson's Correlation Coefficient [^2].  Also known as "Pearson's r", the Pearson Correlation Coffefient is "a measure of linear correlation between two variables X and Y.  It has a value between +1 and -1, where 1 is total positive linear correlation, 0 is no linear correlation, and -1 is total negative linear correlation."  The formula for computing the Pearson's r is given as:
$$\rho_{X,Y} = \frac{cov(X,Y)}{\sigma_X\sigma_Y}$$
This formula brings together the familiar statistical measures of Covariance and Standard Deviation, and can be simplified further to:
$$\rho_{X,Y} = \frac{E[(X - \mu_X)(Y - \mu_Y)]}{\sigma_X\sigma_Y}$$

Spearman's Rank Correlation Coefficient [^3] is another statistical measurement used to determine dependence between two variables.  In contrast to Pearson's r, Spearman's Correlation is a "nonparametric measure" which means that it "makes no assumptions about the probability distributions of the variables being assessed"[^5].  Instead it uses a "Rank Correlation" which simply orders the variables using  "an ordinal association"[^6] allowing for assessment of monotonic relationships.

Spearman's Rank Correlation Coefficient is related to the Pearson Correlation Coefficient in that it is defined as "The Pearson Correlation Coefficient between the ranked variables," and is calculated using deviations.  Mathematically, it is defined as:
$$r_s = \rho_{rg_X,rg_Y} = \frac{cov(rg_X, rg_Y)}{\sigma_{rg_X}\sigma_{rg_Y}}$$
If all ranks are distinct integers, the Spearman Coefficient may be computed using the simpler formula:
$$r_s = 1 - \frac{6\Sigma d_i^2}{n(n^2 -1)}$$
Like the Spearman's Correlation, the Kendall Rank Correlation Coefficient [^4], also known as kendalls' tau, is another non-parametric rank correlation.  Unlike Spearman's, calculation of Kendall's tau is based on sol-called "concordant" and "discordant" pairs, which are defined as agreement, or disagreement, between points in a dataset.  Kendall's tau can be computed as:
$$\tau = \frac{\text{(number of concordant pairs) - (number of discordant pairs)}}{n(n-1)/2}$$
A variant of Kendall's tau, Kendalls's tau-a, can be computed as:
$$\tau_a = \text{(probability of concordant pairs) - (probability of discordant pairs)}$$

## Our Data


The data we will be considering is in the table below
```{r echo=T}
setwd("~/Code/stat-706/")
X1 <- c(0,1,10)
X2 <- c(0,1)
p <- c(0.1, 0.4, 0.2, 0.1, 0.1, 0.1)
d <- cbind(merge(X1, X2), p)
colnames(d) <- c("X1", "X2", "p")

library(knitr)
kable(d, caption = "X1, X2 data")
```

### Pearson's r

```{r}
mX1 <- c(sum(d$p[d$X1 == 0]), sum(d$p[d$X1 == 1]), sum(d$p[d$X1 == 10]))
cat("Marginal X1: ", mX1)
mX2 <- c(sum(d$p[d$X2 == 0]), sum(d$p[d$X2 == 1]))
cat("Marginal X2: ", mX2)
EX_X1 <- sum(d$X1 * d$p)
cat("E[X1]: ", EX_X1)
EX_X2 <- sum(d$X2 * d$p)
cat("E[X2]: ", EX_X2)
EX2_X1 <- sum(d$X1^2 * d$p)
cat("E[X1^2]: ", EX2_X1)
EX2_X2 <- sum(d$X2^2 * d$p)
cat("E[X2^2]: ", EX2_X2)
EX1X2 <- sum(d$X1 * d$X2 * d$p)
cat("Expected X1X2: ", EX1X2)

vX1 <- EX2_X1 - EX_X1^2
cat("Variance X1: ", vX1)
vX2 <- EX2_X2 - EX_X2^2
cat("Variance X2: ", vX2)
sdX1 <- sqrt(vX1)
cat("Std Dev  X1: ", sdX1)
sdX2 <- sqrt(vX2)
cat("Std Dev  X2: ", sdX2)

covX1X2 <- EX1X2 - (EX_X1 * EX_X2)
cat("cov of X1X2: ", covX1X2)

```
$$\rho_{X,Y} = \frac{cov(X,Y)}{\sigma_X\sigma_Y}$$
```{r}
cat("Pearson's: ", covX1X2 / (sdX1 * sdX2))
```
### Spearman's rho
```{r echo=T}
# Spearman values
X2_0p <- c(.1,.4,.2)
X2_1p <- c(.1,.1,.1)
spearman <- cbind(X2_0p, X2_1p)
PrX1_1 <- sum(spearman[1,])
PrX1_2 <- sum(spearman[2,])
PrX1_3 <- sum(spearman[3,])
spearman <- cbind(spearman, c(PrX1_1, PrX1_2, PrX1_3))
rank_X2 <- c(1, 2, 'PrX1')
colnames(spearman) <- rank_X2
PrX2_1 <- sum(spearman[1:3,1])
PrX2_2 <- sum(spearman[1:3,2])
spearman <- rbind(spearman, c(PrX2_1, PrX2_2, 1))
rank_X1 <- c(1, 2, 3, 'PrX2')
rownames(spearman) <- rank_X1

kable(spearman, caption = "Spearman Ranks")

# spearman Expected values
E_rX_1 <- sum(spearman[1:3, 3] * strtoi(rank_X1[1:3]))
cat("E for X1: ", E_rX_1)
E_rX2_1 <- sum(spearman[1:3, 3] * strtoi(rank_X1[1:3])^2)
cat("E^2 for X1: ", E_rX2_1)
E_rX_2 <- sum(spearman[4, 1:2] * strtoi(rank_X2[1:2]))
cat("E for X2: ", E_rX_2)
E_rX2_2 <- sum(spearman[4, 1:2] * strtoi(rank_X2[1:2])^2)
cat("E^2 for X2: ", E_rX2_2)
# Spearman variance
vrX1 <- E_rX2_1 - E_rX_1^2
cat("var(rX1): ", vrX1)
vrX2 <- E_rX2_2 - E_rX_2^2
cat("var(rX2): ", vrX2)
# spearman covariance
E_rX1rX2 <- sum( spearman[1:3,1] * strtoi(rank_X1[1:3]) * strtoi(rank_X2[1]) + 
                 spearman[1:3,2] * strtoi(rank_X1[1:3]) * strtoi(rank_X2[2]) )
covrX1rX2 <- E_rX1rX2 - (E_rX_1 * E_rX_2)
cat("cov(rX1, rX2): ", covrX1rX2)
```
$$r_s = \rho_{rg_{X_1},rg_{X_2}} = \frac{cov(rg_{X_1}, rg_{X_2})}{\sigma_{rg_{X_1}}\sigma_{rg_{X_2}}}$$
```{r}
spearmans_cc <- covrX1rX2 / (sqrt(vrX1) * sqrt(vrX2))
cat("Spearmans: ", spearmans_cc)

```
### Kendall's tau-a
```{r echo=F}
# Kendall's tau-a
kendalls <- mat.or.vec(6,6)
kendalls[,] <- "-"
colnames(kendalls) <- c(paste(d$X1, d$X2))
rownames(kendalls) <- c(paste(d$X1, d$X2))
# There has to be a better way to do this!!
kendalls["0 0", "1 1"] <- 'C'
kendalls["0 0", "10 1"] <- 'C'
kendalls["1 0", "0 1"] <- 'D'
kendalls["1 0", "10 1"] <- 'C'
kendalls["10 0", "0 1"] <- 'D'
kendalls["10 0", "1 1"] <- 'D'
kendalls["0 1", "1 0"] <- 'D'
kendalls["0 1", "10 0"] <- 'D'
kendalls["1 1", "0 0"] <- 'C'
kendalls["1 1", "10 0"] <- 'D'
kendalls["10 1", "0 0"] <- 'C'
kendalls["10 1", "1 0"] <- 'C'

#kable(d, caption = "X1, X2 data")
kable(kendalls, caption = "Kendalls Concordant/Discordant pairs")

p.0.0 <- .1
p.1.0 <- .4
p.10.0 <- .2
p.0.1 <- .1
p.1.1 <- .1
p.10.1 <- .1

c_pairs <- sum(c(p.0.0*p.1.1, p.0.0*p.10.1, p.1.0*p.10.1, p.1.1*p.0.0, p.10.1*p.0.0, p.10.1*p.1.0))
d_pairs <- sum(c(p.1.0*p.0.1, p.10.0*p.0.1, p.10.0*p.1.1, p.0.1*p.1.0, p.0.1*p.10.0, p.1.1*p.10.0))
kendalls_cc <- c_pairs - d_pairs
cat("Probability of Concordant pairs: ", c_pairs)
cat("Probability of Discordant pairs: ", d_pairs)
```
$$\tau_a = \text{(probability of concordant pairs) - (probability of discordant pairs)}$$
```{r}
cat("Kendall's Correlation Coefficient: ", kendalls_cc)
  
```
### Explanation Of Observations
The Pearson's Correlation Coefficient we found was approximately 0.0256, which is so close to zero that we could consider the points in the sample to be unrelated.  The sample we are dealing with, though, is very small.  Our calculations might be improved with a larger sample size.

Our calculations for Spearman's $\rho$ and Kendall's $\tau$ are also quite low,  approxiamtely -0.094 and -0.04, respectively.  They are negative, while Pearson's was positive, but they are all close to enough to zero that, again, we can consider the datapoints in the sample to be unrelated.

### Alternate Distributions For Alternative Results
We have seen that, with the dataset provided, both Spearman's $\rho$ and Kendall's $\tau$ are negative.  Is it possible to construct a dataset in which we have an outcome such that $\rho$ is negative while $\tau$ is positive, or vice-versa?

Using varying probabilites for the same data, I was not able to find a distribution that gave a $\rho$ of one sign and a $\tau$ of another.  For example:

```{r}
# Probabilites
X2_0p <- c(.1,.2,.1) # (0,0), (1,0), (10,0)
X2_1p <- c(.4,.1,.1) # (0,1), (1,1), (10,1)
spearman <- cbind(X2_0p, X2_1p)
PrX1_1 <- sum(spearman[1,])
PrX1_2 <- sum(spearman[2,])
PrX1_3 <- sum(spearman[3,])
spearman <- cbind(spearman, c(PrX1_1, PrX1_2, PrX1_3))
rank_X2 <- c(1, 2, 'PrX1')
colnames(spearman) <- rank_X2
PrX2_1 <- sum(spearman[1:3,1])
PrX2_2 <- sum(spearman[1:3,2])
spearman <- rbind(spearman, c(PrX2_1, PrX2_2, 1))
rank_X1 <- c(1, 2, 3, 'PrX2')
rownames(spearman) <- rank_X1

kable(spearman, caption = "Spearman Ranks")

# spearman Expected values
E_rX_1 <- sum(spearman[1:3, 3] * strtoi(rank_X1[1:3]))
cat("E for X1: ", E_rX_1)
E_rX2_1 <- sum(spearman[1:3, 3] * strtoi(rank_X1[1:3])^2)
cat("E^2 for X1: ", E_rX2_1)
E_rX_2 <- sum(spearman[4, 1:2] * strtoi(rank_X2[1:2]))
cat("E for X2: ", E_rX_2)
E_rX2_2 <- sum(spearman[4, 1:2] * strtoi(rank_X2[1:2])^2)
cat("E^2 for X2: ", E_rX2_2)
# Spearman variance
vrX1 <- E_rX2_1 - E_rX_1^2
cat("var(rX1): ", vrX1)
vrX2 <- E_rX2_2 - E_rX_2^2
cat("var(rX2): ", vrX2)
# spearman covariance
E_rX1rX2 <- sum( spearman[1:3,1] * strtoi(rank_X1[1:3]) * strtoi(rank_X2[1]) + 
                 spearman[1:3,2] * strtoi(rank_X1[1:3]) * strtoi(rank_X2[2]) )
covrX1rX2 <- E_rX1rX2 - (E_rX_1 * E_rX_2)
cat("cov(rX1, rX2): ", covrX1rX2)
spearmans_cc <- covrX1rX2 / (sqrt(vrX1) * sqrt(vrX2))
cat("Spearmans: ", spearmans_cc)
```
#### Kendall's tau
```{r}
p.0.0 <- X2_0p[1] #.1
p.1.0 <- X2_0p[2] #.4
p.10.0 <- X2_0p[3] #.2
p.0.1 <- X2_1p[1] #.1
p.1.1 <- X2_1p[2] #.1
p.10.1 <- X2_1p[3] #.1

c_pairs <- sum(c(p.0.0*p.1.1, p.0.0*p.10.1, p.1.0*p.10.1, p.1.1*p.0.0, p.10.1*p.0.0, p.10.1*p.1.0))
d_pairs <- sum(c(p.1.0*p.0.1, p.10.0*p.0.1, p.10.0*p.1.1, p.0.1*p.1.0, p.0.1*p.10.0, p.1.1*p.10.0))
kendalls_cc <- c_pairs - d_pairs
cat("Probability of Concordant pairs: ", c_pairs)
cat("Probability of Discordant pairs: ", d_pairs)

cat("Kendall's Correlation Coefficient: ", kendalls_cc)
  
```


[^1]: https://en.wikipedia.org/wiki/Correlation_and_dependence
[^2]: https://en.wikipedia.org/wiki/Pearson_correlation_coefficient
[^3]: https://en.wikipedia.org/wiki/Spearman%27s_rank_correlation_coefficient
[^4]: https://en.wikipedia.org/wiki/Kendall_rank_correlation_coefficient
[^5]: https://en.wikipedia.org/wiki/Nonparametric_statistics
[^6]: https://en.wikipedia.org/wiki/Rank_correlation